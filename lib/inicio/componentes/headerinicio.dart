import 'package:flutter/material.dart';
import 'package:backdrop/backdrop.dart';

import 'searchfield.dart';

class HeaderInicio extends StatelessWidget {
  static MediaQueryData _mediaQueryData;
  static double screenWidth;
  static double screenHeight;
  static double defaultSize;
  static Orientation orientation;

  const HeaderInicio({
    key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    _mediaQueryData = MediaQuery.of(context);
    screenWidth = _mediaQueryData.size.width;
    screenHeight = _mediaQueryData.size.height;
    orientation = _mediaQueryData.orientation;
    return BackdropScaffold(
      appBar: BackdropAppBar(
        title: const Text('Centro Rehabilitación')
      ),
      headerHeight: 120.0,
      frontLayer: const Center(
        child: Text("imagen"),
        heightFactor: 10,
      ),
      backLayer: const Center(child: Text('atras'),
      heightFactor: 10,),
    );
  }
}