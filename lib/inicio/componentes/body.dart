import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:tp2_app_medica/Vistas/productos/inicio_producto.dart';
import 'package:tp2_app_medica/Vistas/ventas/inicio_ventas.dart';
import 'package:tp2_app_medica/Vistas/persona/inicio_personas.dart';



class Body extends StatelessWidget {
  static MediaQueryData _mediaQueryData;
  static double screenWidth;
  static double screenHeight;
  static double defaultSize;
  static Orientation orientation;

  String usuario;
  Body({this.usuario});



  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Center(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Flexible(
                    child: Image.asset('images/logo.png', height: 340.0),
                  ),
                  SizedBox(height: 30,),
                  _Pacientes(),
                  SizedBox(height: 30,),
                  _Reservas(),
                  SizedBox(height: 30,),
                  _FichasClinicas(),

                ]
            )
        ),
      ),
    );
  }

  Widget _Pacientes() {
    return StreamBuilder(
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          return ElevatedButton(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 89, vertical: 20),
                child: Text('Clientes',
                    style: TextStyle(
                        fontSize: 20)),
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => InicioPersonas()));
              });
        }
    );
  }

  Widget _Reservas() {
    return StreamBuilder(
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          return ElevatedButton(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 89, vertical: 20),
                child: Text('Productos',
                    style: TextStyle(
                        fontSize: 20)),
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => InicioProducto()));
              });
        }
    );
  }

  Widget _FichasClinicas() {
    return StreamBuilder(
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          return ElevatedButton(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 68, vertical: 20),
                child: Text('Ventas',
                    style: TextStyle(
                        fontSize: 20)),
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => InicioVentas()));
              });
        }
    );
  }
}