import 'package:flutter/cupertino.dart';
import 'package:tp2_app_medica/modelos/persona.dart';
import 'package:tp2_app_medica/modelos/tipo_producto_categoria.dart';


class FichaClinica
{
  int idFichaClinica;
  String motivoConsulta;
  String diagnostico;
  String observacion;
  Persona empleado;
  Persona cliente;
  TipoProducto tipoProducto;
  String fechaDesde;
  String fechaHasta;


  FichaClinica({this.idFichaClinica, this.motivoConsulta, this.diagnostico, this.observacion, this.empleado, this.cliente, this.tipoProducto, this.fechaDesde, this.fechaHasta});
  FichaClinica.empty();

  factory FichaClinica.listarFichaClinicaJson(Map<String, dynamic> elemento) {
    return FichaClinica(
        idFichaClinica: elemento['idFichaClinica'],
        motivoConsulta: (elemento['motivoConsulta'] ?? ''),
        diagnostico: (elemento['diagnostico'] ?? ''),
        observacion: (elemento['observacion'] ?? ''),
        empleado: Persona.personaJson(elemento['idEmpleado']),
        cliente: Persona.personaJson(elemento['idCliente']),
        tipoProducto: TipoProducto.tipoProductoJson(elemento['idTipoProducto']),
        fechaDesde: (elemento['fechaDesde'] ?? ''),
        fechaHasta: (elemento['fechaHasta'] ?? ''),
    );
  }

  factory FichaClinica.fichaClinicaJson(Map<String, dynamic> elemento) {
    return FichaClinica(
      idFichaClinica: elemento['idFichaClinica'],
      motivoConsulta: (elemento['motivoConsulta'] ?? ''),
      diagnostico: (elemento['diagnostico'] ?? ''),
      observacion: (elemento['observacion'] ?? ''),
      empleado: Persona.personaJson(elemento['idEmpleado']),
      cliente: Persona.personaJson(elemento['idCliente']),
      tipoProducto: (elemento['tipoProducto'] ?? ''),
      fechaDesde: (elemento['fechaDesde'] ?? ''),
      fechaHasta: (elemento['fechaHasta'] ?? ''),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "motivoConsulta": motivoConsulta,
      "diagnostico": diagnostico,
      "observacion": observacion,
      "idEmpleado": empleado.idPersona,
      "idCliente": cliente.idPersona,
      "idTipoProducto": tipoProducto.idTipoProducto
    };
  }

  Map<String, dynamic> toJsonUpdate() {
    return {
      "idFichaClinica": idFichaClinica,
      "observacion": observacion,
    };
  }

}