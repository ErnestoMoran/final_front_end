class Producto{
  int codigoProducto;
  String nombre;
  int precioVenta;
  int existencia;

  List<Producto> productos = [];

  Producto({this.codigoProducto,this.nombre,this.precioVenta,this.existencia});
  Producto.empty();

  factory Producto.listarProductoJson(Map<String,dynamic> elemento){
    return Producto(
      codigoProducto: elemento['codigoProducto'],
      nombre: (elemento['nombre'] ?? ''),
      existencia: (elemento['existencia'] ?? ''),
      precioVenta: (elemento['precioVenta'] ?? '')
    );
  }
  Map<String, dynamic> toJson() {
    return {
      "codigoProducto":codigoProducto,
      "nombre": nombre,
      "existencia":existencia,
      "precioVenta":precioVenta
    };
  }

  factory Producto.productoJson(Map<String, dynamic> elemento) {
    if(elemento!=null) {
      return Producto(
          codigoProducto: elemento['codigoProducto'],
          nombre: (elemento['nombre'] ?? ''),
          existencia: (elemento['existencia'] ?? ''),
          precioVenta: (elemento['precioVenta'] ?? '')
      );
    }else{
      return Producto.empty();
    }
  }


  List<Map<String, dynamic>> toJSONEncodable() {
    return productos.map((item) {
      return item.toJson();
    }).toList();
  }

}