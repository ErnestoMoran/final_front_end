import 'package:flutter/material.dart';

class Usuario {
  String idPersona;
  String nombre;
  String apellido;
  String user;

  Usuario({this.idPersona, this.nombre, this.apellido, this.user});
}