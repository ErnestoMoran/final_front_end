import 'package:flutter/cupertino.dart';

class Cliente {
  String ruc;
  String nombreApellido;
  String email;

  List<Cliente> clientes = [];

  Cliente({this.ruc, this.nombreApellido, this.email});
  Cliente.empty();

  factory Cliente.listarClienteJson(Map<String, dynamic> elemento) {
    return Cliente(
        ruc: elemento['ruc'],
        nombreApellido: (elemento['nombreApellido'] ?? ''),
        email: (elemento['email'] ?? ''),
    );
  }

  factory Cliente.clienteJson(Map<String, dynamic> elemento) {
    if(elemento!=null) {
      return Cliente(
          ruc: elemento['ruc'],
          nombreApellido: (elemento['nombreApellido'] ?? ''),
          email: (elemento['email'] ?? ''),
      );
    }else{
      return Cliente.empty();
    }
  }

  Map<String, dynamic> toJson() {
    return {
      "ruc": ruc,
      "nombreApellido": nombreApellido,
      "email": email,
    };
  }

  Map<String, dynamic> toJsonUpdate() {
    return {
      "ruc": ruc,
      "nombreApellido": nombreApellido,
      "email": email,
    };
  }

  List<Map<String, dynamic>> toJSONEncodable() {
    return clientes.map((item) {
      return item.toJson();
    }).toList();
  }


}