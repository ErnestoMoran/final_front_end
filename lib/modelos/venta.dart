import 'package:flutter/cupertino.dart';
import 'package:tp2_app_medica/modelos/producto.dart';

import 'cliente.dart';

class Detalle {
  Producto producto;
  int cantidad;
  int totalDetalle;

  List detalles = [];

  Detalle({this.producto, this.cantidad, this.totalDetalle});
  Detalle.empty();

  factory Detalle.listarDetalleJson(Map<String, dynamic> elemento) {
    return Detalle(
      producto: Producto.productoJson(elemento['producto']),
      cantidad: (elemento['cantidad'] ?? ''),
      totalDetalle: (elemento['totalDetalle'] ?? ''),
    );
  }

  factory Detalle.detalleJson(Map<String, dynamic> elemento) {
    if(elemento!=null) {
      return Detalle(
        producto: elemento['producto'],
        cantidad: (elemento['cantidad'] ?? ''),
        totalDetalle: (elemento['totalDetalle'] ?? ''),
      );
    }else{
      return Detalle.empty();
    }
  }

  Map<String, dynamic> toJson() {
    return {
      "producto": producto,
      "cantidad": cantidad,
      "totalDetalle": totalDetalle,
    };
  }

  Map<String, dynamic> toJsonUpdate() {
    return {
      "producto": producto,
      "cantidad": cantidad,
      "totalDetalle": totalDetalle,
    };
  }

  List<Map<String, dynamic>> toJSONEncodable() {
    return detalles.map((item) {
      return item.toJson();
    }).toList();
  }



}

class Venta
{
  int idCabecera;
  String fecha;
  String numeroFactura;
  Cliente cliente;
  int total;
  List detalles = [];

  List<Venta> ventas = [];

  Venta({this.idCabecera, this.fecha, this.numeroFactura, this.cliente, this.total, this.detalles});
  Venta.empty();

  factory Venta.listarVentaJson(Map<String, dynamic> elemento) {
    return Venta(
        idCabecera: elemento['idCabecera'],
        fecha: (elemento['fecha'] ?? ''),
        numeroFactura: (elemento['numeroFactura'] ?? ''),
        cliente: Cliente.clienteJson(elemento),
        total: (elemento['total'] ?? ''),
        detalles: (elemento['detalles'] ?? ''),
    );
  }

  factory Venta.ventaJson(Map<String, dynamic> elemento) {
    if(elemento!=null) {
      return Venta(
        idCabecera: elemento['idCabecera'],
        fecha: (elemento['fecha'] ?? ''),
        numeroFactura: (elemento['numeroFactura'] ?? ''),
        cliente: (elemento['cliente'] ?? ''),
        total: (elemento['total'] ?? ''),
        detalles: (elemento['detalles'] ?? ''),
      );
    }else{
      return Venta.empty();
    }
  }

  Map<String, dynamic> toJson() {
    return {
      "idCabecera": idCabecera,
      "fecha": fecha,
      "numeroFactura": numeroFactura,
      "cliente": cliente,
      "total": total,
      "detalles": detalles,
    };
  }

  Map<String, dynamic> toJsonUpdate() {
    return {
      "idCabecera": idCabecera,
      "fecha": fecha,
      "numeroFactura": numeroFactura,
      "cliente": cliente,
      "total": total,
      "detalles": detalles,
    };
  }

  List<Map<String, dynamic>> toJSONEncodable() {
    return ventas.map((item) {
      return item.toJson();
    }).toList();
  }


}