import 'package:flutter/cupertino.dart';
import 'package:tp2_app_medica/modelos/persona.dart';


class Reserva{
  int idReserva;
  String fecha;
  String horaInicio;
  String horaFin;
  String fechaHoraCreacion;
  String flagEstado;
  String flagAsistio;
  String observacion;
  String idFichaClinica;
  Persona idCliente;
  Persona idEmpleado;
  String fechaCadena;
  String fechaDesdeCadena;
  String fechaHastaCadena;
  String horaInicioCadena;
  String horaFinCadena;

  Reserva({this.idReserva,this.fecha,this.horaInicio,this.horaFin, this.flagEstado, this.flagAsistio, this.idCliente, this.idEmpleado, this.fechaDesdeCadena, this.fechaHastaCadena, this.horaInicioCadena, this.horaFinCadena, this.observacion, this.fechaCadena, this.fechaHoraCreacion});
  Reserva.empty();

  factory Reserva.reservaJson(Map<String,dynamic> elemento){
    return Reserva(
      idReserva: elemento['idReserva'],
      fecha: (elemento['fecha'] ?? ''),
      horaInicio: (elemento['horaInicio'] ?? ''),
      horaFin: (elemento['horaFin'] ?? ''),
      flagEstado:(elemento['flagEstado'] ?? ''),
      flagAsistio:(elemento['flagAsistio'] ?? ''),
      idCliente: Persona.personaJson(elemento['idCliente']),
      idEmpleado: Persona.personaJson(elemento['idEmpleado']),
      fechaDesdeCadena:(elemento['fechaDesdeCadena'] ?? ''),
      fechaHastaCadena:(elemento['fechaHastaCadena'] ?? ''),
      horaInicioCadena:(elemento['horaInicioCadena'] ?? ''),
      horaFinCadena:(elemento['horaFinCadena'] ?? ''),
      observacion:(elemento['observacion'] ?? ''),
      fechaCadena:(elemento['fechaCadena'] ?? ''),
      fechaHoraCreacion:(elemento['fechaHoraCreacion'] ?? '')
    );
  }

  Map<String, dynamic> toJsonCreate(){
    return {
      "idCliente":idCliente.idPersona,
      "horaInicioCadena":horaInicioCadena,
      "horaFinCadena":horaFinCadena,
      "idEmpleado":idEmpleado.idPersona,
      "fechaCadena":fechaCadena
    };
  }

  Map<String, dynamic> toJsonUpdate(){
    return{
      "idReserva":idReserva,
      "observacion":observacion,
      "flagAsistio":flagAsistio
    };
  }

}
