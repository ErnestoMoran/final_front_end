import 'package:flutter/widgets.dart';
import 'package:tp2_app_medica/inicio/inicio.dart';

final Map<String,WidgetBuilder> routes= {
  PantallaInicio.routeName: (context) => PantallaInicio(),
};