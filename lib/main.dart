import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:tp2_app_medica/inicio/bienvenida/welcome.dart';
import 'package:tp2_app_medica/inicio/inicio.dart';
import 'package:tp2_app_medica/servicios/servicio_clientes.dart';
import 'package:tp2_app_medica/servicios/servicio_ficha_clinica.dart';
import 'package:tp2_app_medica/servicios/servicio_personas.dart';
import 'package:tp2_app_medica/servicios/servicio_productos.dart';
import 'package:tp2_app_medica/servicios/servicio_reservas.dart';
import 'package:tp2_app_medica/servicios/servicio_tipo_producto_categoria.dart';
import 'package:tp2_app_medica/servicios/servicio_ventas.dart';


void setupLocator(){
  GetIt.I.registerLazySingleton(() => ServicioPersona());
  GetIt.I.registerLazySingleton(() => ServicioFichaClinica());
  GetIt.I.registerLazySingleton(() => ServicioReserva());
  GetIt.I.registerLazySingleton(() => ServicioTipoProductoCategoria());
  GetIt.I.registerLazySingleton(() => ServicioCliente());
  GetIt.I.registerLazySingleton(() => ServicioProducto());
  GetIt.I.registerLazySingleton(() => ServicioVenta());

}

void main() {
  setupLocator();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {

    return MaterialApp(
        title: 'App Médica',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.deepPurple,
        ),
        home: PantallaInicio(),
        initialRoute: WelcomeScreen.id,
        routes: {
          WelcomeScreen.id : (context) => WelcomeScreen(),

        },
    );
  }
}
