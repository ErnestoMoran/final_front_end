import 'package:flutter/material.dart';

class EliminarVenta extends StatelessWidget {
  const EliminarVenta({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        title: Text('Alerta'),
        content: Text('¿Estas seguro que quieres eliminar esta venta?'),
        actions: <Widget>[
          TextButton(
            onPressed: (){
              Navigator.of(context).pop(true);
            },
            child: Text('Si'),
          ),
          TextButton(
            onPressed: (){
              Navigator.of(context).pop(false);
            },
            child: Text('No'),
          ),
        ]
    );
  }
}
