import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:tp2_app_medica/modelos/cliente.dart';
import 'package:tp2_app_medica/modelos/venta.dart';
import 'package:tp2_app_medica/servicios/servicio_clientes.dart';
import 'package:tp2_app_medica/servicios/servicio_ventas.dart';


class CrearModificarVenta extends StatefulWidget {

  final int index;
  const CrearModificarVenta({this.index});

  @override
  _CrearModificarVentaState createState() => _CrearModificarVentaState();
}

class _CrearModificarVentaState extends State<CrearModificarVenta> {
  bool get estaEditando => widget.index != null;

  int contadorVenta = 5;



  ServicioVenta get servicioVenta => GetIt.I<ServicioVenta>();
  ServicioCliente get servicioCliente => GetIt.I<ServicioCliente>();

  String mensajeError;
  Venta venta;

  TextEditingController _controladorFactura = TextEditingController();
  TextEditingController _controladorTotal = TextEditingController();
  List<Cliente> _listaClientes;
  Cliente _cliente;
  Cliente _clienteElegido;
  DateTime _fecha;
  List detalles;

  bool _estaCargando = false;

  getCliente(cliente) async {
    _cliente = servicioCliente.getClientesRuc(cliente);
  }


  @override
  void initState() {
    _listaClientes = servicioCliente.getListaClientes();
    super.initState();


    if (estaEditando){

      setState(() {
        _estaCargando = true;
      });

      venta = servicioVenta.getVenta(widget.index);

      if(venta != null){
        setState(() {
          _estaCargando = false;

          _controladorFactura.text = venta.numeroFactura;
          _controladorTotal.text = venta.total.toString();
          _cliente = venta.cliente;
          _fecha = DateTime.parse(venta.fecha);

        });
      }


    }
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(title: Text(estaEditando ? 'Editar Venta' : 'Crear Venta')),
      body: Padding(
        padding: const EdgeInsets.all(12.0),
        child: _estaCargando ? Center(child: CircularProgressIndicator()) : Column(
          children: <Widget>[
            TextField(
              controller: _controladorFactura,
              decoration: const InputDecoration(
                  hintText: 'Factura'
              ),
            ),

            Container(height: 8),

            TextField(
              controller: _controladorTotal,
              decoration: const InputDecoration(
                  hintText: 'Costo total'
              ),
            ),

            Container(height: 8),

            ListTile(
              title: const Text('Cliente'),
              enabled: estaEditando ? false : true,
              trailing:  DropdownButton(
                  items: _listaClientes.map((item) {
                    return DropdownMenuItem(
                      child: Text(item.nombreApellido),
                      value: item.ruc,
                    );
                  }).toList(),
                  onChanged: (newVal) {
                    setState(() {
                      _clienteElegido = newVal;
                      getCliente(_clienteElegido);
                    });
                  },
                  value: _clienteElegido
              ),
            ),


            Container(height: 8),

            Text(_fecha == null ? 'Fecha de nacimiento' : _fecha.toString()),
            TextButton(onPressed: (){
              showDatePicker(context: context,
                  initialDate: _fecha ?? DateTime.now(),
                  firstDate: DateTime(1900),
                  lastDate: DateTime(2222)
              ).then((date) {
                setState(() {
                  _fecha = date;
                });
              });
            },
              child: const Text('Elija una fecha'),
            ),


            Container(height: 16,),

            SizedBox(
              width: double.infinity,
              height: 35,
              child:ElevatedButton( //Este en verdad es mi widget de boton que esta wrapeado en sizedbox pero solo para cambiar el tamaño del boton
                child: const Text('Guardar', style: TextStyle(color: Colors.white)),
                style: ElevatedButton.styleFrom(
                  /* primary: Colors.redAccent, // fijar el color de la caja del boton
                 side: BorderSide(width: 3, color: Colors.brown), // configura los lados del boton
                  elevation: 3, // pone que tan elevado va a estar el boton
                  shape: RoundedRectangleBorder( // Para colocar el radio de boton
                    borderRadius: BorderRadius.circular(50)
                  ),
                  padding: EdgeInsets.all(5),
                  alignment: Alignment.bottomCenter, */
                ),
                onPressed: () async {
                  if (estaEditando) {

                    setState(() {
                      _estaCargando = true;
                    });

                    final venta = Venta(
                      numeroFactura: _controladorFactura.text,
                      total: int.parse(_controladorTotal.text),
                      cliente: _clienteElegido,
                      fecha: _fecha.toString().substring(0,_fecha.toString().indexOf('.')),
                    );
                    final resultado = servicioVenta.editarVenta(widget.index, venta);

                    setState(() {
                      _estaCargando = false;
                    });

                    const titulo = 'Actualizado';
                    final texto = resultado == null ? 'Ocurrió un error' : 'La venta ha sido actualizada';


                    showDialog(context: context,
                        builder: (_) =>
                            AlertDialog(
                              title: Text(titulo),
                              content: Text(texto),
                              actions: <Widget>[
                                TextButton(
                                  child: const Text('Ok'),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                )
                              ],
                            )
                    )
                        .then((data) {
                      Navigator.of(context).pop();
                    });
                  } else {

                    setState(() {
                      _estaCargando = true;
                      ++contadorVenta;
                    });


                    final venta = Venta(
                      numeroFactura: _controladorFactura.text,
                      total: int.parse(_controladorTotal.text),
                      cliente: _clienteElegido,
                      fecha: _fecha.toString().substring(0,_fecha.toString().indexOf('.')),
                    );
                    final resultado = servicioVenta.crearVenta(contadorVenta, venta, detalles);

                    setState(() {
                      _estaCargando = false;
                    });

                    const titulo = 'Creado';
                    final texto = resultado.idCabecera == null ?
                        'Ocurrió un error' : 'La venta ha sido creada';


                    showDialog(context: context,
                        builder: (_) =>
                            AlertDialog(
                              title: Text(titulo),
                              content: Text(texto),
                              actions: <Widget>[
                                TextButton(
                                  child: const Text('Ok'),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                )
                              ],
                            )
                    )
                        .then((data) {
                      if (resultado.idCabecera != null) {
                        Navigator.of(context).pop();
                      }
                    });
                  }
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
