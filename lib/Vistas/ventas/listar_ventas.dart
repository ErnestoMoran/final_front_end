// ignore_for_file: unused_field

import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:tp2_app_medica/Vistas/ventas/crear_modificar_ventas.dart';
import 'package:tp2_app_medica/Vistas/ventas/eliminar_ventas.dart';
import 'package:tp2_app_medica/modelos/venta.dart';
import 'package:tp2_app_medica/servicios/servicio_ventas.dart';

class ListaVentas extends StatefulWidget {

  @override
  State<ListaVentas> createState() => _ListaVentasState();
}

class _ListaVentasState extends State<ListaVentas> {
  ServicioVenta get servicio => GetIt.I<ServicioVenta>();
  List<Venta> _ventas;
  bool _estaCargando = false;

  String formatDateTime(DateTime dateTime){
    return '${dateTime.day}/${dateTime.month}/${dateTime.year}';
  }

  @override
  void initState() {
    servicio.iniciar();
    _fetchVentas();
    super.initState();
  }

  _fetchVentas() {
    setState(() {
      _estaCargando = true;
    });

    _ventas = servicio.getListaVentas();
    setState(() {
      _estaCargando = false;
    });
  }

  _fetchVentasBusqueda(String texto) {
    /*setState(() {
      _estaCargando = true;
    }); */

    _ventas = servicio.getListaVentasBusqueda(texto);

    setState(() {
      _estaCargando = false;
    });
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Lista de Ventas')),
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (builder)=> CrearModificarVenta()))
              .then((_) {
            _fetchVentas();
          });
        },
        child: Icon(Icons.add),
      ), // floatingActionButton
      body: Builder(
          builder: (_) {
            if(_estaCargando) {
              return Center(child: CircularProgressIndicator());
            }

            return ListView.separated(
              itemBuilder: (context, index) {
                return index == 0 ? _barraBusqueda() : _listaElementos(index-1);
              },
              separatorBuilder: (context, index) => Divider(height: 1, color: Colors.deepPurple),
              itemCount: _ventas.length+1,
            );
          }
      ),
    );
  }

  _barraBusqueda(){
    return Padding(
        padding: const EdgeInsets.all(8.0),
        child: TextField(
            decoration: const InputDecoration(
                hintText: 'Buscar...'
            ),
            onChanged: (texto){
              if(texto != ''){
                texto = texto.toLowerCase();
                _fetchVentasBusqueda(texto);
              } else {
                _fetchVentas();
              }
            })
    );
  }

  _listaElementos(index){
    return Dismissible(
      key: ValueKey(_ventas[index].idCabecera),
      direction: DismissDirection.startToEnd,
      onDismissed: (direction) {

      },
      confirmDismiss: (direction) async {
        final resultado = await showDialog(
            context: context,
            builder: (_) => EliminarVenta()
        );

        if(resultado) {
          print(index);
          final eliminarResultado = servicio.eliminarVenta(index);

          var mensaje;
          if (eliminarResultado == true) {
            mensaje = 'La venta ha sido eliminada';
          } else {
            mensaje = 'Ocurrió un error';
          }

          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text(mensaje),
            duration: new Duration(milliseconds: 1000),
          ));

          return eliminarResultado ?? false;

        }


        return resultado;
      },
      background: Container(
        color: Colors.red,
        padding: const EdgeInsets.only(left: 16),
        child: const Align(child: Icon(Icons.delete, color: Colors.white), alignment: Alignment.centerLeft),
      ),
      child: ListTile(
        title: Text(
          "Factura: " + _ventas[index].numeroFactura + "\t Fecha: " + _ventas[index].fecha,
          style: TextStyle(color: Theme.of(context).primaryColor),
        ),
        subtitle: Text('Cliente: ${
            _ventas[index].cliente.nombreApellido} '
            '\Total: ${_ventas[index].total}'),
        onTap: () {
          Navigator.of(context)
              .push(MaterialPageRoute(
              builder: (_) => CrearModificarVenta(index: index)))
              .then((data) {
            _fetchVentas();
          });
        },
      ),
    );

  }
}
