// ignore_for_file: unused_field

import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:tp2_app_medica/Vistas/persona/crear_modificar_persona.dart';
import 'package:tp2_app_medica/Vistas/persona/eliminar_personas.dart';
import 'package:tp2_app_medica/modelos/persona.dart';
import 'package:tp2_app_medica/servicios/servicio_personas.dart';

class ListaPersonas extends StatefulWidget {

  @override
  State<ListaPersonas> createState() => _ListaPersonasState();
}

class _ListaPersonasState extends State<ListaPersonas> {
  ServicioPersona get servicio => GetIt.I<ServicioPersona>();
  List<Persona> _personas;
  bool _estaCargando = false;

  String formatDateTime(DateTime dateTime){
    return '${dateTime.day}/${dateTime.month}/${dateTime.year}';
  }

  @override
  void initState() {
    servicio.iniciar();
    _fetchPersonas();
    super.initState();
  }

  _fetchPersonas() {
    setState(() {
      _estaCargando = true;
    });

    _personas = servicio.getListaPersonas();

    setState(() {
      _estaCargando = false;
    });
  }

  _fetchPersonasBusqueda(String texto) {
    /*setState(() {
      _estaCargando = true;
    }); */

    _personas = servicio.getListaPersonasBusqueda(texto);

    setState(() {
      _estaCargando = false;
    });
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Lista de Personas')),
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (builder)=> CrearModificarPersona()))
              .then((_) {
            _fetchPersonas();
          });
        },
        child: Icon(Icons.add),
      ), // floatingActionButton
      body: Builder(
          builder: (_) {
            if(_estaCargando) {
              return Center(child: CircularProgressIndicator());
            }

            return ListView.separated(
              itemBuilder: (context, index) {
                return index == 0 ? _barraBusqueda() : _listaElementos(index-1);
              },
              separatorBuilder: (context, index) => Divider(height: 1, color: Colors.green),
              itemCount: _personas.length+1,
            );
          }
      ),
    );
  }

  _barraBusqueda(){
    return Padding(
        padding: const EdgeInsets.all(8.0),
        child: TextField(
            decoration: const InputDecoration(
                hintText: 'Buscar...'
            ),
            onChanged: (texto){
              if(texto != ''){
                texto = texto.toLowerCase();
                _fetchPersonasBusqueda(texto);
              } else {
                _fetchPersonas();
              }
            })
    );
  }

  _listaElementos(index){
    print(_personas[index].nombre);
    return Dismissible(
      key: ValueKey(_personas[index].idPersona),
      direction: DismissDirection.startToEnd,
      onDismissed: (direction) {

      },
      confirmDismiss: (direction) async {
        final resultado = await showDialog(
            context: context,
            builder: (_) => EliminarPersona()
        );

        if(resultado) {
          print(index);
          final eliminarResultado = servicio.eliminarPersona(index);

          var mensaje;
          if (eliminarResultado == true) {
            mensaje = 'La persona ha sido eliminada';
          } else {
            mensaje = 'Ocurrió un error';
          }

          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text(mensaje),
            duration: new Duration(milliseconds: 1000),
          ));

          return eliminarResultado ?? false;

        }


        return resultado;
      },
      background: Container(
        color: Colors.red,
        padding: const EdgeInsets.only(left: 16),
        child: const Align(child: Icon(Icons.delete, color: Colors.white), alignment: Alignment.centerLeft),
      ),
      child: ListTile(
        title: Text(
          _personas[index].nombre + " " + _personas[index].apellido,
          style: TextStyle(color: Theme.of(context).primaryColor),
        ),
        subtitle: Text('Fecha de nacimiento ${
            _personas[index].fechaNacimiento}'),
        onTap: () {
          Navigator.of(context)
              .push(MaterialPageRoute(
              builder: (_) => CrearModificarPersona(index: index)))
              .then((data) {
            _fetchPersonas();
          });
        },
      ),
    );

  }
}
