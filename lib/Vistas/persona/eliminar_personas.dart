import 'package:flutter/material.dart';

class EliminarPersona extends StatelessWidget {
  const EliminarPersona({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        title: Text('Alerta'),
        content: Text('¿Estas seguro que quieres eliminar esta persona?'),
        actions: <Widget>[
          TextButton(
            onPressed: (){
              Navigator.of(context).pop(true);
            },
            child: Text('Si'),
          ),
          TextButton(
            onPressed: (){
              Navigator.of(context).pop(false);
            },
            child: Text('No'),
          ),
        ]
    );
  }
}
