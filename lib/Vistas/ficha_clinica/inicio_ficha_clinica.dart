import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tp2_app_medica/Vistas/ficha_clinica/listar_ficha_clinica.dart';
import 'package:tp2_app_medica/inicio/componentes/widget_drawer.dart';

class InicioFichaClinica extends StatelessWidget {
  static MediaQueryData _mediaQueryData;
  static double screenWidth;
  static double screenHeight;
  static double defaultSize;
  static Orientation orientation;

  String usuario;
  InicioFichaClinica({this.usuario});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Fichas Clinicas'),
        ),
        drawer: MenuLateral(),
        body: Center(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Flexible(
                    child: Image.asset('images/logo.png', height: 350),
                  ),
                  SizedBox(height: 20,),
                  _ListarFichas(),
                ]
            )
        ),
      ),
    );
  }

  Widget _ListarFichas() {
    return StreamBuilder(
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          return ElevatedButton(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 70, vertical: 20),
                child: Text('Listar fichas clínicas',
                    style: TextStyle(
                        fontSize: 20)),
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ListaFichasClinicas(usuario: this.usuario)));
              });
        }
    );
  }
}