import 'package:flutter/material.dart';

class EliminarProducto extends StatelessWidget {
  const EliminarProducto({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('Alerta'),
      content: Text('¿Estas seguro que quieres eliminar esta reserva?'),
      actions: <Widget>[
        TextButton(
            onPressed: (){
              Navigator.of(context).pop(true);
            },
            child: Text('Si')
        ),
        TextButton(
            onPressed: (){
              Navigator.of(context).pop(false);
            },
            child: Text('No')
        )
      ],
    );
  }
}
