import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:tp2_app_medica/Vistas/Reservas/listar_reservas.dart';
import 'package:tp2_app_medica/modelos/persona.dart';
import 'package:tp2_app_medica/modelos/reserva.dart';
import 'package:tp2_app_medica/servicios/servicio_reservas.dart';

class ModificarReserva extends StatefulWidget {
  final Reserva reservaEdit;
  ModificarReserva({this.reservaEdit});

  @override
  _ModificarReservaState createState() => _ModificarReservaState();
}

class _ModificarReservaState extends State<ModificarReserva> {

  static const asistencia = <String>[
      'SI',
    'NO',
  ];

  String asistio;


  ServicioReserva get servicioReserva => GetIt.I<ServicioReserva>();

  bool get estaEditando => widget.reservaEdit != null;

  String mensajeError;
  Reserva reserva;

  TextEditingController _controladorNombreCliente = TextEditingController();
  TextEditingController _controladorApellidoCliente = TextEditingController();
  TextEditingController _controladorNombreEmpleado = TextEditingController();
  TextEditingController _controladorApellidoEmpleado = TextEditingController();
  TextEditingController _controladorFecha = TextEditingController();
  TextEditingController _controladorHorario = TextEditingController();
  TextEditingController _controladorObservacion = TextEditingController();
  TextEditingController _controladorAsistencia = TextEditingController();
  DateTime _fecha;

  bool _estaCargando = false;

  @override
  void initState() {
    super.initState();


    if (estaEditando){

      setState(() {
        _estaCargando = true;
      });


        reserva=widget.reservaEdit;
        _controladorFecha.text=reserva.fecha;
        _controladorNombreCliente.text=reserva.idCliente.nombre+" "+reserva.idCliente.apellido;
        _controladorNombreEmpleado.text=reserva.idEmpleado.nombre+" "+reserva.idEmpleado.apellido;
        _controladorHorario.text=reserva.horaInicio+" - "+reserva.horaFin;
        _controladorObservacion.text=reserva.observacion;
        if(widget.reservaEdit.flagAsistio=="S"){
          asistio=asistencia[0];
        }else if(widget.reservaEdit.flagAsistio=="N"){
          asistio=asistencia[1];
        }

    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Editar Reserva')),
      body: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Column(
          children: <Widget>[
            TextField(
              controller: _controladorNombreCliente,
              decoration: InputDecoration(
                hintText: "Cliente"
              ),
              enabled: false
            ),

            Container(height: 8),

            TextField(
              controller: _controladorNombreEmpleado,
              decoration: InputDecoration(
                hintText: "Empleado"
              ),
              enabled: false
            ),

            Container(height: 8),

            TextField(
              controller: _controladorFecha,
              decoration: InputDecoration(
                hintText: "Fecha"
              ),
              enabled: false
            ),

            Container(height: 8),

            TextField(
              controller: _controladorHorario,
              decoration: InputDecoration(
                hintText: 'Horario'
              ),
              enabled: false
            ),

            Container(height: 8),

            TextField(
              controller: _controladorObservacion,
              decoration: InputDecoration(
                hintText: 'Observacion'
              ),
            ),

            ListTile(
              title: const Text('Asistencia'),
              trailing:  DropdownButton(
                items: asistencia.map((item) {
                  return DropdownMenuItem(
                    child: Text(item),
                    value: item,
                  );
                }).toList(),
                onChanged: (newVal) {
                  setState(() {
                    asistio = newVal;
                  });
                },
                value: asistio,
              ),
            ),


            Container(height: 8),

            SizedBox(
              width: double.infinity,
              height: 35,
              child: ElevatedButton(
                child: const Text('Guardar',style: TextStyle(color: Colors.white)),
                style: ElevatedButton.styleFrom(),
                onPressed: () async {
                  final reserva=Reserva(
                    idReserva: widget.reservaEdit.idReserva,
                    observacion: _controladorObservacion.text,
                    flagAsistio: asistio[0],
                  );
                  print(reserva.toJsonUpdate().toString());
                  final resultado = await servicioReserva.editarReserva(reserva);
                  print(reserva.flagAsistio);

                  const titulo = 'Actualizado';
                  final texto = resultado.error ? (resultado.errorMessage ??
                      'Ocurrió un error') : 'La persona ha sido actualizada';

                  showDialog(context: context,
                      builder: (_) =>
                          AlertDialog(
                            title: Text(titulo),
                            content: Text(texto),
                            actions: <Widget>[
                              TextButton(
                                child: const Text('Ok'),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                              )
                            ],
                          )
                  )
                      .then((data) {
                    Navigator.of(context).pop();
                  });
                },
              ),
            )

          ],
        ),
      ),
    );
  }
}
