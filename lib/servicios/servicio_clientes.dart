import 'dart:convert';
import 'package:tp2_app_medica/modelos/cliente.dart';
import 'package:shared_preferences/shared_preferences.dart';


class ServicioCliente {

  List _clientes = [];
  SharedPreferences prefs;

  static const headers = {
    'Content-type': 'application/json'
  };

  static final CLIENTES = <Cliente>[
    Cliente(nombreApellido: "Ivonne Rolon",
        email: "ivonne@gmail.com",
        ruc: "123456-7"),
    Cliente(nombreApellido: "Ernesto Moran",
        email: "ernes@gmail.com",
        ruc: "123456-7"),
    Cliente(nombreApellido: "Fernando Caballero",
        email: "fernando@gmail.com",
        ruc: "123456-7"),
    Cliente(nombreApellido: "Natalia Cardozo",
        email: "natalia@gmail.com",
        ruc: "123456-7"),
    Cliente(nombreApellido: "Benjamin Racchi",
        email: "benjamin@gmail.com",
        ruc: "123456-7")
  ];

  iniciar() async {
    prefs = await SharedPreferences.getInstance();
    _clientes = CLIENTES;
    prefs.setString("clientes", json.encode(_clientes));
  }


  List<Cliente> getListaClientes() {
    if (prefs.get("clientes") != null) {
      final jsonData = json.decode(prefs.get('clientes'));
      final clientes = <Cliente>[];
      for (var elemento in jsonData) {
        clientes.add(Cliente.listarClienteJson(elemento));
      }
      return clientes;
    }
  }

  Cliente crearCliente(Cliente cliente) {
    if (prefs.get("clientes") != null) {
      final jsonData = json.decode(prefs.get('clientes'));
      final clientes = <Cliente>[];
      for (var elemento in jsonData) {
        clientes.add(Cliente.listarClienteJson(elemento));
      }
      clientes.add(cliente);
      prefs.setString("clientes", json.encode(clientes));
      return cliente;
    }
  }

  Cliente getCliente(int index) {
    if (prefs.get("clientes") != null) {
      final jsonData = json.decode(prefs.get('clientes'));
      final clientes = <Cliente>[];
      for (var elemento in jsonData) {
        clientes.add(Cliente.listarClienteJson(elemento));
      }
      Cliente clienteAux = clientes[index];

      return clienteAux;
    }
  }


  List<Cliente> getListaClientesBusqueda(String texto) {
    if (prefs.get("clientes") != null) {
      final jsonData = json.decode(prefs.get("clientes"));
      final clientes = <Cliente>[];
      for (var elemento in jsonData) {
        clientes.add(Cliente.listarClienteJson(elemento));
      }
      final clientesAux = <Cliente>[];
      for(int i=0;i<clientes.length;i++) {
        final nombreCompleto = clientes[i].nombreApellido.toLowerCase();
        if (nombreCompleto.contains(texto.toLowerCase())) {
          clientesAux.add(clientes[i]);
        }
      }
      return clientesAux;
    }
  }

  Cliente getClientesRuc(String texto) {
    if (prefs.get("clientes") != null) {
      final jsonData = json.decode(prefs.get("clientes"));
      final clientes = <Cliente>[];
      for (var elemento in jsonData) {
        clientes.add(Cliente.listarClienteJson(elemento));
      }
      for(int i=0;i<clientes.length;i++) {
        if (clientes[i].ruc == texto) {
          return clientes[i];
        }
      }
    }
  }

  Cliente editarCliente(int index, Cliente cliente) {
    if (prefs.get("clientes") != null) {
      final jsonData = json.decode(prefs.get('clientes'));
      final clientes = <Cliente>[];
      for (var elemento in jsonData) {
        clientes.add(Cliente.listarClienteJson(elemento));
      }
      cliente.ruc = clientes[index].ruc;
      clientes[index] = cliente;
      prefs.setString("clientes", json.encode(clientes));
      return cliente;
    }
  }

  bool eliminarCliente(int index) {
    if (prefs.get("clientes") != null) {
      final jsonData = json.decode(prefs.get('clientes'));
      final clientes = <Cliente>[];
      for (var elemento in jsonData) {
        clientes.add(Cliente.listarClienteJson(elemento));
      }
      clientes.removeAt(index);
      return true;
    }
    return false;
  }

}
