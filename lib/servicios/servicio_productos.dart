import 'dart:convert';
import 'package:tp2_app_medica/modelos/producto.dart';
import 'package:shared_preferences/shared_preferences.dart';


class ServicioProducto {

  List _productos = [];
  SharedPreferences prefs;

  static const headers = {
    'Content-type': 'application/json'
  };

  static final PRODUCTOS = <Producto>[
    Producto(
        nombre: "Mesa",
        codigoProducto: 1,
        existencia: 5,
        precioVenta: 200000
    ),
    Producto(
        nombre: "Silla",
        codigoProducto: 2,
        existencia: 20,
        precioVenta: 80000
    ),
    Producto(
        nombre: "TV",
        codigoProducto: 3,
        existencia: 3,
        precioVenta: 1000000
    ),
    Producto(
        nombre: "Cocina",
        codigoProducto: 4,
        existencia: 2,
        precioVenta: 1500000
    ),
  ];


  iniciar() async {
    prefs = await SharedPreferences.getInstance();
    _productos = PRODUCTOS;
    prefs.setString("productos", json.encode(_productos));
  }


  List<Producto> getListaProductos() {
    if (prefs.get("productos") != null) {
      final jsonData = json.decode(prefs.get('productos'));
      final productos = <Producto>[];
      for (var elemento in jsonData) {
        productos.add(Producto.listarProductoJson(elemento));
      }
      return productos;
    }
  }

  Producto crearProducto(int id, Producto producto) {
    if (prefs.get("productos") != null) {
      final jsonData = json.decode(prefs.get('productos'));
      final productos = <Producto>[];
      for (var elemento in jsonData) {
        productos.add(Producto.listarProductoJson(elemento));
      }
      producto.codigoProducto = id;
      productos.add(producto);
      prefs.setString("productos", json.encode(productos));
      return producto;
    }
  }

  Producto getProducto(int index) {
    if (prefs.get("productos") != null) {
      final jsonData = json.decode(prefs.get('productos'));
      final productos = <Producto>[];
      for (var elemento in jsonData) {
        productos.add(Producto.listarProductoJson(elemento));
      }
      Producto productoAux = productos[index];

      return productoAux;
    }
  }


  List<Producto> getListaProductosBusqueda(String texto) {
    print(texto);
    if (prefs.get("productos") != null) {
      final jsonData = json.decode(prefs.get("productos"));
      final productos = <Producto>[];
      for (var elemento in jsonData) {
        productos.add(Producto.listarProductoJson(elemento));
      }
      final productosAux = <Producto>[];
      for(int i=0;i<productos.length;i++) {
        final nombreCompleto = productos[i].nombre.toLowerCase();
        if (nombreCompleto.contains(texto.toLowerCase())) {
          productosAux.add(productos[i]);
        }
      }
      return productosAux;
    }
  }

  Producto editarProducto(int index, Producto producto) {
    if (prefs.get("productos") != null) {
      final jsonData = json.decode(prefs.get('productos'));
      final productos = <Producto>[];
      for (var elemento in jsonData) {
        productos.add(Producto.listarProductoJson(elemento));
      }
      producto.codigoProducto = productos[index].codigoProducto;
      productos[index] = producto;
      prefs.setString("productos", json.encode(productos));
      return producto;
    }
  }

  bool eliminarProducto(int index) {
    if (prefs.get("productos") != null) {
      final jsonData = json.decode(prefs.get('productos'));
      final productos = <Producto>[];
      for (var elemento in jsonData) {
        productos.add(Producto.listarProductoJson(elemento));
      }
      productos.removeAt(index);
      return true;
    }
    return false;
  }

}
