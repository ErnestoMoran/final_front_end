import 'dart:convert';
import 'package:flutter/widgets.dart';
import 'package:tp2_app_medica/modelos/api_response.dart';
import 'package:tp2_app_medica/modelos/cliente.dart';
import 'package:tp2_app_medica/modelos/producto.dart';
import 'package:tp2_app_medica/modelos/venta.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';


class ServicioVenta {

  List _ventas = [];
  SharedPreferences prefs;

  static const String api = 'https://equipoyosh.com/stock-nutrinatalia';

  static const headers = {
    'Content-type': 'application/json'
  };

  static final PRODUCTOS = <Producto>[
    Producto(
        nombre: "Mesa",
        codigoProducto: 1,
        existencia: 5,
        precioVenta: 200000
    ),
    Producto(
        nombre: "Silla",
        codigoProducto: 2,
        existencia: 20,
        precioVenta: 80000
    ),
    Producto(
        nombre: "TV",
        codigoProducto: 3,
        existencia: 3,
        precioVenta: 1000000
    ),
    Producto(
        nombre: "Cocina",
        codigoProducto: 4,
        existencia: 2,
        precioVenta: 1500000
    ),
  ];

  static final CLIENTES = <Cliente>[
    Cliente(nombreApellido: "Ivonne Rolon",
        email: "ivonne@gmail.com",
        ruc: "123456-7"),
    Cliente(nombreApellido: "Ernesto Moran",
        email: "ernes@gmail.com",
        ruc: "123456-7"),
    Cliente(nombreApellido: "Fernando Caballero",
        email: "fernando@gmail.com",
        ruc: "123456-7"),
    Cliente(nombreApellido: "Natalia Cardozo",
        email: "natalia@gmail.com",
        ruc: "123456-7"),
    Cliente(nombreApellido: "Benjamin Racchi",
        email: "benjamin@gmail.com",
        ruc: "123456-7")
  ];


  static final DETALLES = <Detalle>[
    Detalle(
      producto: PRODUCTOS[0],
      cantidad: 2,
      totalDetalle: PRODUCTOS[0].precioVenta * 2,
    ),
    Detalle(
      producto: PRODUCTOS[1],
      cantidad: 4,
      totalDetalle: PRODUCTOS[1].precioVenta * 4,
    ),
    Detalle(
      producto: PRODUCTOS[2],
      cantidad: 1,
      totalDetalle: PRODUCTOS[1].precioVenta,
    ),
  ];


  static final VENTAS = <Venta>[
    Venta(
      idCabecera: 1,
      cliente: CLIENTES[0],
      detalles: DETALLES,
      fecha: "2021-02-05",
      numeroFactura: "001-001-001",
      total: DETALLES[0].totalDetalle + DETALLES[1].totalDetalle + DETALLES[2].totalDetalle ,
    ),
  ];

  iniciar() async {
    prefs = await SharedPreferences.getInstance();
    setListaVenta(VENTAS);
    print("paso la parte de insertar ventas");
  }


  List<Venta> getListaVentas() {
    if (prefs.get("ventas") != null) {
      final jsonData = json.decode(prefs.get('ventas'));
      final ventas = <Venta>[];
      for (var elemento in jsonData) {
        ventas.add(Venta.listarVentaJson(elemento));
      }
      return ventas;
    }
  }

  void setListaVenta(List<Venta> ventas){
    for(int i=0;i<ventas.length;i++){
      _ventas.add((ventas[i].toJson()));
    }
    prefs.setString("ventas", json.encode(_ventas));
  }

  void setListaDetalles(int index, List<Detalle> detalles){
    for(int i=0;i<detalles.length;i++){
      _ventas[index].add((detalles[i].toJson()));
    }
  }


  Venta crearVenta(int id, Venta venta, List detalles) {
    if (prefs.get("ventas") != null) {
      final jsonData = json.decode(prefs.get('ventas'));
      final ventas = <Venta>[];
      for (var elemento in jsonData) {
        ventas.add(Venta.listarVentaJson(elemento));
      }
      venta.idCabecera = id;
      for(int i=0;i<detalles.length;i++){
        venta.detalles.add((detalles[i].toJson()));
      }
      ventas.add(venta);
      prefs.setString("ventas", json.encode(ventas));
      return venta;
    }
  }

  Venta getVenta(int index) {
    if (prefs.get("ventas") != null) {
      final jsonData = json.decode(prefs.get('ventas'));
      final ventas = <Venta>[];
      for (var elemento in jsonData) {
        ventas.add(Venta.listarVentaJson(elemento));
      }
      Venta ventaAux = ventas[index];

      return ventaAux;
    }
  }


  List<Venta> getListaVentasBusqueda(String texto) {
    print(texto);
    if (prefs.get("ventas") != null) {
      final jsonData = json.decode(prefs.get("ventas"));
      final ventas = <Venta>[];
      for (var elemento in jsonData) {
        ventas.add(Venta.listarVentaJson(elemento));
      }
      final ventasAux = <Venta>[];
      for(int i=0;i<ventas.length;i++) {
        final nombreCompleto = ventas[i].cliente.nombreApellido.toLowerCase() + " " + ventas[i].fecha.toLowerCase() + " " + ventas[i].numeroFactura.toLowerCase();
        if (nombreCompleto.contains(texto.toLowerCase())) {
          ventasAux.add(ventas[i]);
        }
      }
      return ventasAux;
    }
  }

  Venta editarVenta(int index, Venta venta) {
    if (prefs.get("ventas") != null) {
      final jsonData = json.decode(prefs.get('ventas'));
      final ventas = <Venta>[];
      for (var elemento in jsonData) {
        ventas.add(Venta.listarVentaJson(elemento));
      }
      venta.idCabecera = ventas[index].idCabecera;
      ventas[index] = venta;
      prefs.setString("ventas", json.encode(ventas));
      return venta;
    }
  }

  bool eliminarVenta(int index) {
    if (prefs.get("ventas") != null) {
      final jsonData = json.decode(prefs.get('ventas'));
      final ventas = <Venta>[];
      for (var elemento in jsonData) {
        ventas.add(Venta.listarVentaJson(elemento));
      }
      ventas.removeAt(index);
      return true;
    }
    return false;
  }

}
