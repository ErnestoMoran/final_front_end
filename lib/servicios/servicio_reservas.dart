import 'dart:convert';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:tp2_app_medica/modelos/api_response.dart';
import 'package:http/http.dart' as http;
import 'package:tp2_app_medica/modelos/reserva.dart';

class ServicioReserva{

  static const String api = 'https://equipoyosh.com/stock-nutrinatalia';
  static const headers = {
    'Content-type': 'application/json',
    'usuario':'usuario1'
  };

  Future<APIResponse<List<Reserva>>> getListaReserva(){
    return http.get(Uri.parse(api+'/reserva'))
        .then((data){
          if(data.statusCode == 200){
            final Map<String, dynamic> map=json.decode(data.body);
            final jsonData = map["lista"];
            final reservas= <Reserva>[];
            for(var elemento in jsonData){
              reservas.add(Reserva.reservaJson(elemento));
            }
            reservas.sort((a,b) {
              var adate = a.fechaHoraCreacion;
              var bdate = b.fechaHoraCreacion;
              return -adate.compareTo(bdate);
            });
            reservas.last.idReserva;
            return APIResponse<List<Reserva>>(data: reservas);
          }
          return APIResponse<List<Reserva>>(error: true, errorMessage: 'Ocurrio un error');

    }).catchError((error) => APIResponse<List<Reserva>>(error: true, errorMessage: error.toString()));
  }

  Future<APIResponse<Reserva>> editarReserva(Reserva reserva){
    return http.put(api+'/reserva',headers: headers,body: json.encode(reserva.toJsonUpdate()))
        .then((data) {
          if(data.statusCode == 204){
            final jsonData=json.decode(data.body);
            return APIResponse<Reserva>(data: Reserva.reservaJson(jsonData));
          }
          return APIResponse<Reserva>(error: true, errorMessage: data.reasonPhrase.toString());
    }).catchError((error) => APIResponse<Reserva>(error: true, errorMessage: error.toString()));
  }

  Future<APIResponse<bool>> eliminarReserva(int idReserva){
    return http.delete(api + '/reserva/'+ idReserva.toString(),headers: headers)
        .then((data) {
          print(data.statusCode);
       if(data.statusCode == 200 ){
         return APIResponse<bool>(data: true);
       }
       return APIResponse<bool>(error: true, errorMessage: data.reasonPhrase.toString());
    }).catchError((error) => APIResponse<bool>(error: true, errorMessage: error.toString()));
  }

  Future<APIResponse<List<Reserva>>> agendaReservaLibresOcupados(int idEmpleado,String fecha){
    return http.get(Uri.parse(api+'/persona/${idEmpleado}/agenda?fecha=${fecha}'))
        .then((data){
      if(data.statusCode == 200){
        final map=json.decode(data.body);
        final reservas= <Reserva>[];
        for(var elemento in map){
          reservas.add(Reserva.reservaJson(elemento));
        }
        reservas.last.idReserva;
        return APIResponse<List<Reserva>>(data: reservas);
      }
      return APIResponse<List<Reserva>>(error: true, errorMessage: 'Ocurrio un error');
    }).catchError((error) => APIResponse<List<Reserva>>(error: true, errorMessage: error.toString()));

    }

  Future<APIResponse<List<Reserva>>> agendaReservaLibres(int idEmpleado,String fecha){
    return http.get(Uri.parse(api+'/persona/${idEmpleado}/agenda?fecha=${fecha}&disponible=S'))
        .then((data){
      if(data.statusCode == 200){
        final List<dynamic> map=json.decode(data.body);
        final reservas= <Reserva>[];
        for(var elemento in map){
          reservas.add(Reserva.reservaJson(elemento));
        }
        reservas.last.idReserva;
        return APIResponse<List<Reserva>>(data: reservas);
      }
      return APIResponse<List<Reserva>>(error: true, errorMessage: 'Ocurrio un error');
    }).catchError((error) => APIResponse<List<Reserva>>(error: true, errorMessage: error.toString()));

  }

  Future<APIResponse<List<Reserva>>> agendaReservaMedicoFecha(){

  }

  Future<APIResponse<List<Reserva>>> agendaReservaMedicoDosFechas(){

  }

  Future<APIResponse<List<Reserva>>> agendaReservaCliente(int idCliente){
    return http.get(Uri.parse(api+'/reserva?ejemplo={"idCliente":{"idPersona":${idCliente}}}'))
        .then((data){
      if(data.statusCode == 200){
        final Map<String, dynamic> map=json.decode(data.body);
        final jsonData = map["lista"];
        final reservas= <Reserva>[];
        for(var elemento in jsonData){
          reservas.add(Reserva.reservaJson(elemento));
        }
        reservas.last.idReserva;
        return APIResponse<List<Reserva>>(data: reservas);
      }
      return APIResponse<List<Reserva>>(error: true, errorMessage: 'Ocurrio un error');
    }).catchError((error) => APIResponse<List<Reserva>>(error: true, errorMessage: error.toString()));

  }

  Future<APIResponse<Reserva>> crearReserva(Reserva reserva){
    return http.post(api+'/reserva',headers: headers, body: json.encode(reserva.toJsonCreate()))
      .then((data) {
        print(reserva.toJsonCreate().toString());
      if (data.statusCode == 201) {
        final jsonData = json.decode(data.body);
        return APIResponse<Reserva>(data: Reserva.reservaJson(jsonData));
      }
      return APIResponse<Reserva>(error: true, errorMessage: data.statusCode.toString());

    }).catchError((error) => APIResponse<Reserva>(error: true, errorMessage: error.toString()));
  }
}