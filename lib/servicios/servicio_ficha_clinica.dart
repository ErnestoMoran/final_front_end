import 'dart:convert';

import 'package:tp2_app_medica/modelos/api_response.dart';
import 'package:tp2_app_medica/modelos/ficha_clinica.dart';
import 'package:http/http.dart' as http;

class ServicioFichaClinica {

  static const String api = 'https://equipoyosh.com/stock-nutrinatalia';

  static const headers = {
    'Content-type': 'application/json',
    'usuario': 'usuario1'
  };

  Future<APIResponse<List<FichaClinica>>> getListaFichasClinicas(){
    return http.get(Uri.parse(api + '/fichaClinica'))
        .then((data) {
      if(data.statusCode == 200) {
        final Map<String, dynamic> map = json.decode(data.body);
        final jsonData = map["lista"];
        final fichasClinicas = <FichaClinica>[];
        for(var elemento in jsonData) {
          fichasClinicas.add(FichaClinica.listarFichaClinicaJson(elemento));
        }
        fichasClinicas.last.idFichaClinica;
        return APIResponse<List<FichaClinica>>(data: fichasClinicas);
      }
      return APIResponse<List<FichaClinica>>(error: true, errorMessage: 'Ocurrio un error');
    }).catchError((error) => APIResponse<List<FichaClinica>>(error: true, errorMessage: error.toString()));
  }


  Future<APIResponse<FichaClinica>> getFichaClinica(int id){
    return http.get(Uri.parse(api + '/fichaClinica/' + id.toString()))
        .then((data) {
      if(data.statusCode == 200) {
        final jsonData = json.decode(data.body);
        return APIResponse<FichaClinica>(data: FichaClinica.fichaClinicaJson(jsonData));
      }
      return APIResponse<FichaClinica>(error: true, errorMessage: 'Ocurrio un error');
    }).catchError((error) => APIResponse<FichaClinica>(error: true, errorMessage: error.toString()));
  }

  Future<APIResponse<FichaClinica>> crearFichaClinica(FichaClinica fichaClinica){
    return http.post(api + '/fichaClinica',headers: headers, body: json.encode(fichaClinica.toJson()))
        .then((data) {
          print(fichaClinica.toJson().toString());
      if(data.statusCode == 201) {
        final jsonData = json.decode(data.body);
        return APIResponse<FichaClinica>(data: FichaClinica.fichaClinicaJson(jsonData));
      }
      return APIResponse<FichaClinica>(error: true, errorMessage: data.statusCode.toString());
    }).catchError((error) => APIResponse<FichaClinica>(error: true, errorMessage: error.toString()));
  }

  Future<APIResponse<FichaClinica>> editarFichaClinica(FichaClinica fichaClinica){
    return http.put(api + '/fichaClinica',headers: headers, body: json.encode(fichaClinica.toJsonUpdate()))
        .then((data) {
      if(data.statusCode == 204) {
        final jsonData = json.decode(data.body);
        return APIResponse<FichaClinica>(data: FichaClinica.fichaClinicaJson(jsonData));
      }
      print(data.body);
      return APIResponse<FichaClinica>(error: true, errorMessage: data.reasonPhrase.toString());
    }).catchError((error) => APIResponse<FichaClinica>(error: true, errorMessage: error.toString()));
  }

  Future<APIResponse<bool>> eliminarFichaClinica(int idFichaClinica){
    return http.delete(api + '/fichaClinica/' + idFichaClinica.toString(),headers: headers)
        .then((data) {
      if(data.statusCode == 200) {
        return APIResponse<bool>(data: true);
      }
      return APIResponse<bool>(error: true, errorMessage: data.reasonPhrase.toString());
    }).catchError((error) => APIResponse<bool>(error: true, errorMessage: error.toString()));
  }

}