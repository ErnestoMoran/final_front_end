import 'dart:convert';
import 'package:flutter/widgets.dart';
import 'package:tp2_app_medica/modelos/api_response.dart';
import 'package:tp2_app_medica/modelos/persona.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';


class ServicioPersona {

  List _personas = [];
  SharedPreferences prefs;

  static const String api = 'https://equipoyosh.com/stock-nutrinatalia';

  static const headers = {
    'Content-type': 'application/json'
  };

  static final PERSONAS = <Persona>[
    Persona(nombre: "Ivonne",
        apellido: "Rolon",
        cedula: "123456",
        email: "ivonne@gmail.com",
        fechaNacimiento: "2021-08-01",
        idPersona: 1,
        ruc: "123456-7",
        telefono: "1234567",
        tipoPersona: null),
    Persona(nombre: "Ernesto",
        apellido: "Moran",
        cedula: "123456",
        email: "ernes@gmail.com",
        fechaNacimiento: "2021-08-01",
        idPersona: 2,
        ruc: "123456-7",
        telefono: "1234567",
        tipoPersona: null),
    Persona(nombre: "Fernando",
        apellido: "Caballero",
        cedula: "123456",
        email: "fernando@gmail.com",
        fechaNacimiento: "2021-08-01",
        idPersona: 3,
        ruc: "123456-7",
        telefono: "1234567",
        tipoPersona: null),
    Persona(nombre: "Natalia",
        apellido: "Cardozo",
        cedula: "123456",
        email: "natalia@gmail.com",
        fechaNacimiento: "2021-08-01",
        idPersona: 4,
        ruc: "123456-7",
        telefono: "1234567",
        tipoPersona: null),
    Persona(nombre: "Benjamin",
        apellido: "Racchi",
        cedula: "123456",
        email: "benjamin@gmail.com",
        fechaNacimiento: "2021-08-01",
        idPersona: 5,
        ruc: "123456-7",
        telefono: "1234567",
        tipoPersona: null),
  ];

  iniciar() async {
    prefs = await SharedPreferences.getInstance();
    _personas = PERSONAS;
    prefs.setString("personas", json.encode(_personas));
  }


  List<Persona> getListaPersonas() {
    if (prefs.get("personas") != null) {
      final jsonData = json.decode(prefs.get('personas'));
      final personas = <Persona>[];
      for (var elemento in jsonData) {
        personas.add(Persona.listarPersonaJson(elemento));
      }
      return personas;
    }
  }

  Persona crearPersona(int id, Persona persona) {
    if (prefs.get("personas") != null) {
      final jsonData = json.decode(prefs.get('personas'));
      final personas = <Persona>[];
      for (var elemento in jsonData) {
        personas.add(Persona.listarPersonaJson(elemento));
      }
      persona.idPersona = id;
      personas.add(persona);
      prefs.setString("personas", json.encode(personas));
      return persona;
    }
  }

  Persona getPersona(int index) {
    if (prefs.get("personas") != null) {
      final jsonData = json.decode(prefs.get('personas'));
      final personas = <Persona>[];
      for (var elemento in jsonData) {
        personas.add(Persona.listarPersonaJson(elemento));
      }
      Persona personaAux = personas[index];

      return personaAux;
    }
  }


  List<Persona> getListaPersonasBusqueda(String texto) {
    print(texto);
    if (prefs.get("personas") != null) {
      final jsonData = json.decode(prefs.get("personas"));
      final personas = <Persona>[];
      for (var elemento in jsonData) {
        personas.add(Persona.listarPersonaJson(elemento));
      }
      final personasAux = <Persona>[];
      for(int i=0;i<personas.length;i++) {
        final nombreCompleto = personas[i].nombre.toLowerCase() + " " + personas[i].apellido.toLowerCase();
        if (nombreCompleto.contains(texto.toLowerCase())) {
          personasAux.add(personas[i]);
        }
      }
      return personasAux;
    }
  }

  Persona editarPersona(int index, Persona persona) {
    if (prefs.get("personas") != null) {
      final jsonData = json.decode(prefs.get('personas'));
      final personas = <Persona>[];
      for (var elemento in jsonData) {
        personas.add(Persona.listarPersonaJson(elemento));
      }
      persona.idPersona = personas[index].idPersona;
      personas[index] = persona;
      prefs.setString("personas", json.encode(personas));
      return persona;
    }
  }

  bool eliminarPersona(int index) {
    if (prefs.get("personas") != null) {
      final jsonData = json.decode(prefs.get('personas'));
      final personas = <Persona>[];
      for (var elemento in jsonData) {
        personas.add(Persona.listarPersonaJson(elemento));
      }
      personas.removeAt(index);
      return true;
    }
    return false;
  }


  Future<APIResponse<List<String>>> getUsuariosSistema() {
    return http.get(Uri.parse(api + '/persona?' +
        json.encode('ejemplo={"soloUsuariosDelSistema":true}')))
        .then((data) {
      if (data.statusCode == 200) {
        final Map<String, dynamic> map = json.decode(data.body);
        List<dynamic> jsonData = map["lista"];
        final usuarios = <String>[];

        for (var elemento in jsonData) {
          usuarios.add(elemento['usuarioLogin'] ?? '');
          print(usuarios[0]);
        }
        return APIResponse<List<String>>(
            data: usuarios, errorMessage: '${data.statusCode}');
      } else {
        return APIResponse<List<String>>(
            error: true, errorMessage: 'Error 500');
      }
    }).catchError((error) =>
        APIResponse<List<String>>(error: true, errorMessage: error.toString()));
  }

  Future<APIResponse<List<Persona>>> getMedicos() {
    return http.get(
        Uri.parse(api + '/persona?ejemplo={"soloUsuariosDelSistema":true}'))
        .then((data) {
      if (data.statusCode == 200) {
        final Map<String, dynamic> map = json.decode(data.body);
        List<dynamic> jsonData = map["lista"];
        final usuarios = <Persona>[];
        print("Los usuarios del sistema son: ");
        print(usuarios.length);
        for (var elemento in jsonData) {
          usuarios.add(Persona.listarPersonaJson(elemento));
        }
        return APIResponse<List<Persona>>(
            data: usuarios, errorMessage: '${data.statusCode}');
      } else {
        return APIResponse<List<Persona>>(
            error: true, errorMessage: 'Error 500');
      }
    }).catchError((error) => APIResponse<List<Persona>>(
        error: true, errorMessage: error.toString()));
  }


}
